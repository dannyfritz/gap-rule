import * as test from "tape";
import * as fs from "fs";
import * as path from "path";
import {
  CampsiteJson,
  listings, reservations, search,
  idToCampsite, isOverlapping, isGapped,
} from "../src/gaprule";

const json = JSON.parse(fs.readFileSync(path.join(__dirname, "./fixture/test-case.json"), { encoding: "utf8" })) as CampsiteJson;

test("idToCampsite", (t) => {
  const campsiteJson = JSON.parse(fs.readFileSync(path.join(__dirname, "../tests/fixture/test-case.json"), { encoding: "utf8" })) as CampsiteJson;
  t.deepEqual(
    idToCampsite(campsiteJson.campsites, 1),
    { id: 1, name: "Cozy Cabin" },
  );
  t.equal(
    idToCampsite(campsiteJson.campsites, 7),
    undefined,
  );
  t.end();
});

test("isOverlapping", (t) => {
  t.equal(
    isOverlapping(["2018-12-01", "2018-12-15"], ["2018-12-18", "2018-12-26"]),
    false,
  );
  t.equal(
    isOverlapping(["2018-12-01", "2018-12-15"], ["2018-12-15", "2018-12-26"]),
    true,
  );
  t.equal(
    isOverlapping(["2018-12-01", "2018-12-17"], ["2018-12-15", "2018-12-26"]),
    true,
  );
  t.equal(
    isOverlapping(["2018-12-01", "2018-12-17"], ["2018-11-15", "2018-11-26"]),
    false,
  );
  t.equal(
    isOverlapping(["2018-12-01", "2018-12-17"], ["2018-11-15", "2018-12-26"]),
    true,
  );
  t.equal(
    isOverlapping(["2018-12-01", "2018-12-17"], ["2018-12-02", "2018-12-15"]),
    true,
  );
  t.equal(
    isOverlapping(["2018-12-01", "2018-12-17"], ["2018-12-01", "2018-12-17"]),
    true,
  );
  t.end();
});

test("isGapped", (t) => {
  t.equal(
    isGapped(1, ["2018-06-04", "2018-06-06"], ["2018-06-01", "2018-06-03"]),
    false,
  );
  t.equal(
    isGapped(1, ["2018-06-04", "2018-06-06"], ["2018-06-08", "2018-06-10"]),
    true,
  );
  t.equal(
    isGapped(1, ["2018-06-04", "2018-06-06"], ["2018-06-01", "2018-06-01"]),
    false,
  );
  t.equal(
    isGapped(1, ["2018-06-04", "2018-06-06"], ["2018-06-02", "2018-06-03"]),
    false,
  );
  t.equal(
    isGapped(1, ["2018-06-04", "2018-06-06"], ["2018-06-07", "2018-06-09"]),
    false,
  );
  t.equal(
    isGapped(1, ["2018-06-04", "2018-06-06"], ["2018-06-01", "2018-06-02"]),
    true,
  );
  t.equal(
    isGapped(1, ["2018-06-04", "2018-06-06"], ["2018-06-07", "2018-06-10"]),
    false,
  );
  t.end();
});

test("listing", (t) => {
  t.deepEqual(
    listings(json),
    ["Cozy Cabin", "Comfy Cabin", "Rustic Cabin", "Rickety Cabin", "Cabin in the Woods"],
  );
  t.end();
});

test("reservations", (t) => {
  t.deepEqual(
    reservations(json),
    [
      {
        campsite: { id: 1, name: "Cozy Cabin" }, campsiteId: 1, startDate: "2018-06-01", endDate: "2018-06-03",
      }, {
        campsite: { id: 1, name: "Cozy Cabin" }, campsiteId: 1, startDate: "2018-06-08", endDate: "2018-06-10",
      }, {
        campsite: { id: 2, name: "Comfy Cabin" }, campsiteId: 2, startDate: "2018-06-01", endDate: "2018-06-01",
      }, {
        campsite: { id: 2, name: "Comfy Cabin" }, campsiteId: 2, startDate: "2018-06-02", endDate: "2018-06-03",
      }, {
        campsite: { id: 2, name: "Comfy Cabin" }, campsiteId: 2, startDate: "2018-06-07", endDate: "2018-06-09",
      }, {
        campsite: { id: 3, name: "Rustic Cabin" }, campsiteId: 3, startDate: "2018-06-01", endDate: "2018-06-02",
      }, {
        campsite: { id: 3, name: "Rustic Cabin" }, campsiteId: 3, startDate: "2018-06-08", endDate: "2018-06-09",
      }, {
        campsite: { id: 4, name: "Rickety Cabin" }, campsiteId: 4, startDate: "2018-06-07", endDate: "2018-06-10",
      },
    ],
  );
  t.end();
});

test("search", (t) => {
  t.deepEqual(
    search(json),
    [
      {
        id: 2,
        name: "Comfy Cabin",
      },
      {
        id: 4,
        name: "Rickety Cabin",
      },
      {
        id: 5,
        name: "Cabin in the Woods",
      },
    ],
  );
  t.end();
});
