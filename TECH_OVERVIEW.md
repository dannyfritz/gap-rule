# Technical Overview

## General

Given a search duration and a list of reservations:
* Return each campsite that:
  * Search duration and all reservation durations do not overlap
  * Search duration and all reservation durations do not have a 1 day gap

## Duration Collisions

For this program, a gap is considered a single night.

There are a number of cases to consider when comparing 2 durations such as `a` & `b`:
```
  0-1-2-3-4-5-6-7-8-9-10
a         |###|
b |###|
gap of -2+ nights
✅

  0-1-2-3-4-5-6-7-8-9-10
a         |###|
b   |###|
gap of -1 night
❌

  0-1-2-3-4-5-6-7-8-9-10
a         |###|
b     |###|
gap of -0 nights
✅

  0-1-2-3-4-5-6-7-8-9-10
a         |###|
b       |###|
overlapping
❌

  0-1-2-3-4-5-6-7-8-9-10
a       |#######|
b         |###|
overlapping
❌

  0-1-2-3-4-5-6-7-8-9-10
a         |###|
b         |###|
overlapping
❌

  0-1-2-3-4-5-6-7-8-9-10
a         |###|
b       |#######|
overlapping
❌

  0-1-2-3-4-5-6-7-8-9-10
a         |###|
b           |###|
overlapping
❌

  0-1-2-3-4-5-6-7-8-9-10
a         |###|
b             |###|
gap of 0 nights
✅

  0-1-2-3-4-5-6-7-8-9-10
a         |###|
b               |###|
gap of 1 night
❌

  0-1-2-3-4-5-6-7-8-9-10
a         |###|
b                 |###|
gap of 2+ nights
✅
```

These cases can be generalized into 4 categories:
* overlapping
* 1 night gap
* 2+ nights gap
* no gap

### Overlapping

Perform an [AABB](https://en.wikipedia.org/wiki/Minimum_bounding_box#Axis-aligned_minimum_bounding_box) check to see if the durations overlap.
If they overlap, a reservation cannot be made.

### 1 Night Gap

If you ensure your durations do not overlap, check how far apart your durations are in days.
If it is 1 day, a reservation cannot be made.

### 0 days and 2+ days

If your durations do not overlap and are not separated by 1 day, a reservation can be made.

## Assumptions

* `CampsiteId`'s in reservations must be valid and exist in `campsites[]`.
* The JSON file is not malformed.
* Dates are in the format `YYYY-MM-DD` as a `String`.