/* eslint-disable no-undef */
import groupBy = require("lodash.groupby");
import areRangesOverlapping = require("date-fns/are_ranges_overlapping");
import differenceInDays = require("date-fns/difference_in_days");
import addDays = require("date-fns/add_days");
import isAfter = require("date-fns/is_after");
import isEqual = require("date-fns/is_equal");
// import addDays = require("date-fns/add_days");
/* eslint-enable no-undef */
/* eslint-disable */
type StringDate = string;
type StringDuration = [StringDate, StringDate];
type CampsiteId = number;

export interface CampsiteJson {
  search: {
    startDate: StringDate,
    endDate: StringDate,
  },
  campsites: Array<{
    id: CampsiteId,
    name: String,
  }>,
  reservations: Array<{
    campsiteId: CampsiteId,
    startDate: StringDate,
    endDate: StringDate,
  }>
}
/* eslint-enable */
/* eslint-disable no-undef */
export const idToCampsite = (data: CampsiteJson["campsites"], id: CampsiteId) =>
  data.find(cs => cs.id === id)!;

export const isOverlapping = (a: StringDuration, b: StringDuration) =>
  areRangesOverlapping(a[0], addDays(a[1], 1), b[0], addDays(b[1], 1));

export const isGapped = (gap: number, a: StringDuration, b: StringDuration) => {
  let prior;
  let latter;
  if (isAfter(b[0], a[1]) || isEqual(b[0], a[1])) {
    prior = addDays(a[1], 1);
    [latter] = b;
  } else {
    prior = addDays(b[1], 1);
    [latter] = a;
  }
  const distance = differenceInDays(latter, prior);
  return distance > 0 && distance <= gap;
};

export const listings = (data: CampsiteJson) =>
  data.campsites.map(cs => cs.name);

export const reservations = (data: CampsiteJson) =>
  data.reservations.map(r => ({
    campsite: idToCampsite(data.campsites, r.campsiteId),
    ...r,
  }));

export const search = (data: CampsiteJson) : CampsiteJson["campsites"] => {
  const campsiteReservations = groupBy(data.reservations, r => r.campsiteId);
  const searchDate: StringDuration = [data.search.startDate, data.search.endDate];
  return data.campsites
    .map(cs => ({
      reservations: campsiteReservations[cs.id] || [],
      ...cs,
    }))
    .filter(cs =>
      cs.reservations.every(r =>
        !isOverlapping(searchDate, [r.startDate, r.endDate])))
    .filter(cs =>
      cs.reservations.every(r =>
        !isGapped(1, searchDate, [r.startDate, r.endDate])))
    .map(cs => ({
      id: cs.id,
      name: cs.name,
    }));
};
