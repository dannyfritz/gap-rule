"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const groupBy = require("lodash.groupby");
const areRangesOverlapping = require("date-fns/are_ranges_overlapping");
const differenceInDays = require("date-fns/difference_in_days");
const addDays = require("date-fns/add_days");
const isAfter = require("date-fns/is_after");
const isEqual = require("date-fns/is_equal");
exports.idToCampsite = (data, id) => data.find(cs => cs.id === id);
exports.isOverlapping = (a, b) => areRangesOverlapping(a[0], addDays(a[1], 1), b[0], addDays(b[1], 1));
exports.isGapped = (gap, a, b) => {
    let prior;
    let latter;
    if (isAfter(b[0], a[1]) || isEqual(b[0], a[1])) {
        prior = addDays(a[1], 1);
        [latter] = b;
    }
    else {
        prior = addDays(b[1], 1);
        [latter] = a;
    }
    const distance = differenceInDays(latter, prior);
    return distance > 0 && distance <= gap;
};
exports.listings = (data) => data.campsites.map(cs => cs.name);
exports.reservations = (data) => data.reservations.map(r => (Object.assign({ campsite: exports.idToCampsite(data.campsites, r.campsiteId) }, r)));
exports.search = (data) => {
    const campsiteReservations = groupBy(data.reservations, r => r.campsiteId);
    const searchDate = [data.search.startDate, data.search.endDate];
    return data.campsites
        .map(cs => (Object.assign({ reservations: campsiteReservations[cs.id] || [] }, cs)))
        .filter(cs => cs.reservations.every(r => !exports.isOverlapping(searchDate, [r.startDate, r.endDate])))
        .filter(cs => cs.reservations.every(r => !exports.isGapped(1, searchDate, [r.startDate, r.endDate])))
        .map(cs => ({
        id: cs.id,
        name: cs.name,
    }));
};
