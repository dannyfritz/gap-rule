# `gaprule`

`gaprule` is tool for searching and inspecting reservation data.
A "gap" is when 2 reservations have a number of nights in between.
These gaps are undesirable because they are much harder to fill to collect revenue.

## [Technical Overview](TECH_OVERVIEW.md)


## Development

```sh
git clone git@gitlab.com:dannyfritz/gap-rule.git
cd gap-rule
npm install
```

Do not modify `.js` files. Only modify `.ts` files and run `npm run build`.

### Run Everything

```sh
npm test
```

Runs
* `npm run lint`
* `npm run build`
* `npm run tests`

### Run Everything in Watch Mode (Recommended)

```sh
npm run dev
```

### Tests

Integration & Unit tests are written in the [tests](./tests) folder.

## Installation

Prerequisites:
* [Node.js](https://nodejs.org/) is required.

To Install:
```sh
npm install -g https://gitlab.com/dannyfritz/gap-rule
```

This will download and install the `gaprule` command on your system.

## Usage

```sh
gaprule -h
  
  gaprule <command>

  Commands:
    gaprule listings      List campsites
    gaprule reservations  List current reservations
    gaprule search        Search for availability

  Options:
    --version   Show version number                                    [boolean]
    -f, --file  JSON File to Use                                      [required]
    -h, --help  Show help                                              [boolean]
```

### Input File Format

You can view an example file [here](./tests/fixture/test-case.json).

```json
{
  "search": {
    "startDate": StringDate,
    "endDate": StringDate,
  },
  "campsites": [{
    "id": CampsiteId,
    "name": String,
  }],
  "reservations": [{
    "campsiteId": CampsiteId,
    "startDate": StringDate,
    "endDate": StringDate,
  }]
}
```

* `StringDate` is a `String` with the format `YYYY-MM-DD`.
* `CampsiteId` is a `Number` with a valid `campsites[].id`

### Show Listings

```sh
gaprule listings -f data.json
```

### Show Reservations

```sh
gaprule reservations -f data.json
```

### Search

```sh
gaprule search -f data.json
```

## [MIT License](LICENSE)