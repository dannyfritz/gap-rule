#!/usr/bin/env node
import * as yargs from "yargs";
import * as fs from "fs";
import {
  CampsiteJson, listings, reservations, search,
} from "../src/gaprule";

const { argv, showHelp } = yargs
  .command("listings", "List campsites")
  .command("reservations", "List current reservations")
  .command("search", "Search for availability")
  .demandCommand()
  .alias("f", "file")
  .nargs("f", 1)
  .describe("f", "JSON File to Use")
  .demandOption(["f"])
  .help("h")
  .alias("h", "help");

const command = argv._[0];
const jsonFile = argv.f;

const json = JSON.parse(fs.readFileSync(jsonFile, { encoding: "utf8" })) as CampsiteJson;

if (command === "listings") {
  process.stdout.write(listings(json).join("\n"));
} else if (command === "reservations") {
  process.stdout.write(
    reservations(json)
      .map(cs => `${cs.campsite.name}: ${cs.startDate} - ${cs.endDate}`)
      .join("\n"),
  );
} else if (command === "search") {
  process.stdout.write(search(json).map(cs => cs.name).join("\n"));
} else {
  process.stderr.write(`"${command}" is an unknown command\n\n`);
  showHelp();
  process.exit(1);
}
process.stdout.write("\n");
