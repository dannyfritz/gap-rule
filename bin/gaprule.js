#!/usr/bin/env node
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const yargs = require("yargs");
const fs = require("fs");
const gaprule_1 = require("../src/gaprule");
const { argv, showHelp } = yargs
    .command("listings", "List campsites")
    .command("reservations", "List current reservations")
    .command("search", "Search for availability")
    .demandCommand()
    .alias("f", "file")
    .nargs("f", 1)
    .describe("f", "JSON File to Use")
    .demandOption(["f"])
    .help("h")
    .alias("h", "help");
const command = argv._[0];
const jsonFile = argv.f;
const json = JSON.parse(fs.readFileSync(jsonFile, { encoding: "utf8" }));
if (command === "listings") {
    process.stdout.write(gaprule_1.listings(json).join("\n"));
}
else if (command === "reservations") {
    process.stdout.write(gaprule_1.reservations(json)
        .map(cs => `${cs.campsite.name}: ${cs.startDate} - ${cs.endDate}`)
        .join("\n"));
}
else if (command === "search") {
    process.stdout.write(gaprule_1.search(json).map(cs => cs.name).join("\n"));
}
else {
    process.stderr.write(`"${command}" is an unknown command\n\n`);
    showHelp();
    process.exit(1);
}
process.stdout.write("\n");
